//
//  Utilities.swift
//  TimerDemo
//
//  Created by Florentin on 11/09/2020.
//  Copyright © 2020 Florentin Lupascu. All rights reserved.
//

import Foundation

private let formatter: DateComponentsFormatter = {
    let formatter =  DateComponentsFormatter()
    formatter.allowedUnits = [.hour, .minute, .second]
    formatter.maximumUnitCount = 3
    formatter.unitsStyle = .short
    return formatter
}()

extension Date {
    func formattedIntervalSince(_ date: Date) -> String {
        return formatter.string(from: date, to: self) ?? String()
    }
}
