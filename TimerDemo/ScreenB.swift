//
//  ScreenB.swift
//  TimerDemo
//
//  Created by Florentin on 11/09/2020.
//  Copyright © 2020 Florentin Lupascu. All rights reserved.
//

import UIKit
import CoreFoundation

class ScreenB: UIViewController {
    
    @IBOutlet weak var timerLabel: UILabel!
    
    private var timer: Timer?
    var startDate: Date!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTimeLabel()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            self?.updateTimeLabel()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }

    @IBAction func goScreenC(_ sender: UIButton) {
        guard
            let navigationController = navigationController,
            let screenC = navigationController.storyboard?.instantiateViewController(withIdentifier: "ScreenC") as? ScreenC
            else { return }

        screenC.startDate = startDate
        navigationController.pushViewController(screenC, animated: true)
    }

    @objc func updateTimeLabel() {
        timerLabel.text =  "Elapsed time: " + Date().formattedIntervalSince(startDate)
    }
}
