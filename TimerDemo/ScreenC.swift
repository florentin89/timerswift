//
//  ScreenC.swift
//  TimerDemo
//
//  Created by Florentin on 11/09/2020.
//  Copyright © 2020 Florentin Lupascu. All rights reserved.
//

import UIKit
import CoreFoundation

class ScreenC: UIViewController {
    
    @IBOutlet weak var timerLabel: UILabel!
    
    var startDate: Date!
    private let endDate = Date()

    override func viewDidLoad() {
        super.viewDidLoad()
        timerLabel.text = "Duration: " + endDate.formattedIntervalSince(startDate)
    }
}
