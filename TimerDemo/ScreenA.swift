//
//  ScreenA.swift
//  TimerDemo
//
//  Created by Florentin on 11/09/2020.
//  Copyright © 2020 Florentin Lupascu. All rights reserved.
//

import UIKit
import CoreFoundation

class ScreenA: UIViewController {
    
    @IBOutlet var timerLabel: UILabel!
    
    private var timer: Timer?
    private let startDate = Date()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTimeLabel()

        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            self?.updateTimeLabel()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }

    @IBAction func goScreenB(_ sender: UIButton) {
        guard
            let navigationController = navigationController,
            let screenB = navigationController.storyboard?.instantiateViewController(withIdentifier: "ScreenB") as? ScreenB
            else { return }

        screenB.startDate = startDate
        navigationController.pushViewController(screenB, animated: true)
    }
    
    @objc func updateTimeLabel() {
        timerLabel.text = "Elapsed time: " + Date().formattedIntervalSince(startDate)
    }
}
