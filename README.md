# Timer Demo

**Version 1.0.0**

**Swift 5.1**

This project was created in Swift 5.1 and represent a Timer which is keeping the count of seconds when the user navigate up and down on different screens.

##                                                            VIEW SCREENSHOTS !

![Screenshot 1](Demo/demo.gif)

## Contributors
@ Florentin Lupascu <lupascu_florentin@yahoo.com>

## License & Copyright
© Florentin Lupascu, TimerDemo

Licensed under the [MIT License](LICENSE).
